<?php

class Person{
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
        $this->fullname = $firstName .' ' . $middleName . ' ' . $lastName . '';
    }

    public function printName(){
        return "Your full name is $this->fullname.";
    }
};

$person = new Person('Senku', '', 'Ishigami');

class Developer extends Person{
    public function printName(){
        return  'Your name is '. $this->fullname . 'and you are a developer.';
    }
}

$developer = new Developer('John', 'Finch', 'Smith');

class Engineer extends Person{
    public function printName(){
        return  'Your are an engineer named '. $this->fullname . '.';
    }
}

$engineer = new Engineer('Harold', 'Myers', 'Reese');

?>